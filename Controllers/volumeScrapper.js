const puppeteer = require("puppeteer");
exports.default = async function pup(keywordLocation) {
  const args = [
    "--no-sandbox",
    "--disable-setuid-sandbox",
    "--disable-infobars",
    "--window-position=0,0",
    "--ignore-certifcate-errors",
    "--ignore-certifcate-errors-spki-list",
    '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36"'
  ];
  const options = {
    args,
    headless: true,
    ignoreHTTPSErrors: true,
    userDataDir: "./tmp",
    //for piping chromium console output to this NodeJS console
    dumpio: true
  };

  const browser = await puppeteer.launch(options);
  console.log("Keyword Received: ", keywordLocation);

  const page = await browser.newPage();

  await page.goto("https://searchvolume.io/", {
    waitUntil: "networkidle2"
  });
  await page.waitFor(20000);

//   await page.waitForSelector("button > span#count");

  await page.evaluate(keyword => {
    document.querySelector("form > div > textarea#input").value = keyword;
    // document.querySelector("#submit").click();
  }, keywordLocation);
  //problem causing:
  //document.querySelector("#spinner-verify")
  //need to configure some listner onHide....
  await page.waitFor(5000);

  await page.evaluate(() => document.querySelector("#submit").click());
  await page.waitForSelector("#table > tbody > tr > td:nth-child(2)");

  let volume = await page.evaluate(
    () =>
      document.querySelector("#table > tbody > tr > td:nth-child(2)").innerText
  );
  volume = volume.replace(/,/g, "");
  volume = parseInt(volume, 10);
  console.log("Volume Received: ", volume);

  browser.close();
  return volume;
};
