const volumeScrapper = require("./Controllers/volumeScrapper").default;

const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

app.get("/", (req, res) => {
  let startDate = new Date();
  startDate.setDate(startDate.getDate() - 30);
  const volume = volumeScrapper(req.query.keyword);
  volume
    .then(
      volume => res.send({ keyword: req.query.keyword, volume: volume }),
      volume
    )
    .catch(err => {
      console.log("error occured while calling volumeScrapper", err);
    });
});
